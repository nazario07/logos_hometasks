package hometask20;

import lombok.SneakyThrows;

import static hometask20.Main.number;
import static java.lang.Thread.sleep;

public class RunnableThread implements Runnable {


    @SneakyThrows
    @Override
    public void run() {
        int[] arr = new int[number];
        arr[0] = 1;
        arr[1] = 1;
        for (int i = 2; i < number; i++) {
            arr[i] = arr[i - 1] + arr[i - 2];
        }
        System.out.print("The reverse order: ");
        for (int i = number - 1; i >= 0; i--) {
            System.out.print(arr[i] + " ");
            sleep(1000);
        }
    }
}




