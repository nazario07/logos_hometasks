package hometask20;

import java.util.Scanner;

public class Main {
    public static Scanner scanner = new Scanner(System.in);
    public static int number;

    public static void main(String[] args) throws InterruptedException {
        System.out.println("Enter how many Fibonacci numbers you want to see: ");
        number = scanner.nextInt();
        MyThread myThread = new MyThread();
        myThread.start();
        myThread.join();
        Thread runnableThread = new Thread(new RunnableThread());
        runnableThread.start();
    }
}
