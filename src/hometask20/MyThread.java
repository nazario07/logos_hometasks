package hometask20;

import lombok.SneakyThrows;

import static hometask20.Main.number;

public class MyThread extends Thread {

    @SneakyThrows
    @Override
    public void run() {
        int n0 = 1;
        int n1 = 1;
        int n2;
        System.out.print("The ordinary order: " + n0 + " ");
        sleep(1000);
        System.out.print(n1 + " ");
        sleep(1000);
        for (int i = 3; i <= number; i++) {
            n2 = n0 + n1;
            System.out.print(n2 + " ");
            sleep(1000);
            n0 = n1;
            n1 = n2;
        }
        System.out.println();
    }
}
