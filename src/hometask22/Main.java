package hometask22;

import java.util.Comparator;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        OperationWithDigits maxValue = (a, b, c) -> Stream.of(a,b,c).max(Comparator.comparing(x->x)).get();
      //  OperationWithDigits maxValue = (a, b, c) -> Math.max(a,Math.max(b,c));
        System.out.println(maxValue.operation(7, 2, 3));

        OperationWithDigits averageValue = (a, b, c) -> (a + b + c) / 3;
        System.out.println(averageValue.operation(1, 2, 3));

        OperationWithDigits sumOfNumbers = (a, b, c) -> a + b + c;
        System.out.println(sumOfNumbers.operation(1, 2, 3));
    }
}
