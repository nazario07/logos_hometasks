package hometask22;

@FunctionalInterface
public interface OperationWithDigits {
    public int operation(int a, int b, int c);
}
