package hometask21.task2;

import java.time.LocalDate;


public class Main {

    public static void main(String[] args) {
        DateForConvert date = new DateForConvert();
        System.out.println(date);
        System.out.println("Converted to LocalDate: "+ date.convertToLocalDate());
        System.out.println("Converted to LocalTime: "+ date.convertToLocalTime());
        System.out.println("Converted to LocalDateTime: "+ date.convertToLocalDateTime());


    }
}
