package hometask21.task2;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.time.LocalDate;

public class DateForConvert extends Date {

    public LocalDate convertToLocalDate() {
        return new java.sql.Date(this.getTime()).toLocalDate();
    }

    public LocalTime convertToLocalTime() {
        return new java.sql.Time(this.getTime()).toLocalTime();
    }

    public LocalDateTime convertToLocalDateTime() {
        return new java.sql.Timestamp(this.getTime()).toLocalDateTime();
    }
}
