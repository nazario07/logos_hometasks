package hometask21.task1;


 class Car {
    @WriterAnnotation
     String model;
    @WriterAnnotation
     int price;
    int power;
    @WriterAnnotation
     int maxSpeed;

     @Override
     public String toString() {
         return "Car{" +
                "model='" + model + '\'' +
                ", price=" + price +
                ", power=" + power +
                ", maxSpeed=" + maxSpeed +
                '}';
     }

     public Car(String model, int price, int power, int maxSpeed) {
        this.model = model;
        this.price = price;
        this.power = power;
        this.maxSpeed = maxSpeed;
    }
}

