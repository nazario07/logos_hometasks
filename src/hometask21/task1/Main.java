package hometask21.task1;

import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class Main {

    public static void main(String[] args) throws IllegalAccessException, IOException {
        Car car = new Car("Ferrari", 10000, 220, 380);
        writeInTheFile(car, car.getClass());
    }

    static void writeInTheFile(Car car, Class<? extends Car> aClass) throws IllegalAccessException, IOException {
        FileOutputStream outputStream = new FileOutputStream("E:\\Logos\\src\\hometask21\\base.txt");
        Field[] fields = aClass.getDeclaredFields();
        System.out.println("Fields to write: ");
        for (Field field : fields) {
            Annotation[] annotations = field.getDeclaredAnnotations();
            for (Annotation annotation : annotations) {
                if (annotation.annotationType().equals(WriterAnnotation.class)) {
                    outputStream.write(field.getName().getBytes());
                    outputStream.write(" ".getBytes());
                    outputStream.write(field.get(car).toString().getBytes());
                    outputStream.write("\n".getBytes());
                    System.out.println(field.get(car));
                }
            }
        }
        outputStream.flush();
        outputStream.close();
    }
}
