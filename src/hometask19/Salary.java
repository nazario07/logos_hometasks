package hometask19;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@Getter
@Setter
public class Salary implements Serializable {
    private int sal;

    @Override
    public String toString() {
        return "" + sal;
    }
}
