package hometask19;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Employee implements Serializable {
    private String name;
    private int id;
    transient Salary sal;

    @Override
    public String toString() {
        return "Employee{" +
               "name=" + name +
               ", id=" + id +
               ", salary=" + getSal() +
               '}';
    }
}
