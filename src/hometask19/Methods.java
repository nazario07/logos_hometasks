package hometask19;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Methods {
    public static void serializeEmployee(Employee employee) throws IOException {
            FileOutputStream fileOutputStream = new FileOutputStream("E:\\Logos\\src\\hometask19\\serialize.txt");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(employee);
            objectOutputStream.close();
            fileOutputStream.close();
            System.out.println("Employee was serialized.");
    }

    public static void serializeEmployeeList(List<Employee> employeeList) throws IOException {
            FileOutputStream fileOutputStream = new FileOutputStream("E:\\Logos\\src\\hometask19\\serialize.txt");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeInt(employeeList.size());
            for (Employee employee : employeeList) {
                objectOutputStream.writeObject(employee);
            }
            objectOutputStream.close();
            fileOutputStream.close();
            System.out.println("Employs list was serialized.");
    }

    public static Employee deserializeEmployee() throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream("E:\\Logos\\src\\hometask19\\serialize.txt");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        Employee employee = (Employee) objectInputStream.readObject();
        objectInputStream.close();
        fileInputStream.close();
        return employee;
    }

    public static List<Employee> deserializeEmployeeList() throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream("E:\\Logos\\src\\hometask19\\serialize.txt");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        int count = objectInputStream.readInt();
        List<Employee> employeeListRes = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            employeeListRes.add((Employee) objectInputStream.readObject());
        }
        objectInputStream.close();
        fileInputStream.close();
        return employeeListRes;
    }
}
