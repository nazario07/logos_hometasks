package hometask19;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main  {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Employee employee1 = new Employee("Nazar Tsykhuliak", 19960707, new Salary(1600));
        Employee employee2 = new Employee("Oksana Tsykhuliak", 19961212, new Salary(1400));
        Employee employee3 = new Employee("Ivan Tsykhuliak", 19691302, new Salary(1300));
        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(employee1);
        employeeList.add(employee2);
        employeeList.add(employee3);

       Methods.serializeEmployeeList(employeeList);

        List<Employee> employeeList1 = Methods.deserializeEmployeeList();
        System.out.println(employeeList1);


    }
}
