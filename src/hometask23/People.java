package hometask23;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class People {

    private String name;
    private int age;
    private Sex sex;
}
