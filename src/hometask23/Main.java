package hometask23;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<People> peopleList = new ArrayList<>();
        peopleList.add(new People("Nazar", 25, Sex.MAN));
        peopleList.add(new People("Oksana Ts", 24, Sex.WOMAN));
        peopleList.add(new People("Andrew", 50, Sex.MAN));
        peopleList.add(new People("Ivan", 53, Sex.MAN));
        peopleList.add(new People("Ivan", 57, Sex.MAN));
        peopleList.add(new People("Maria", 53, Sex.WOMAN));
        peopleList.add(new People("Vira", 50, Sex.WOMAN));
        peopleList.add(new People("Marik", 25, Sex.MAN));
        peopleList.add(new People("Yulia", 24, Sex.WOMAN));
        peopleList.add(new People("Aksana F", 25, Sex.WOMAN));
        peopleList.add(new People("Taras", 27, Sex.MAN));
        peopleList.add(new People("Ivan Ts", 30, Sex.MAN));

        System.out.println("Choose men conscripts (18 to 27 years old): ");
        peopleList.stream()
                .filter(people -> people.getSex() == Sex.MAN)
                .filter(people -> people.getAge() >= 18 && people.getAge() <= 27)
                .forEach(System.out::println);
        System.out.println("----------------------------------------");
        System.out.println("Find the average age of men: ");
        peopleList.stream()
                .filter(people -> people.getSex() == Sex.MAN)
                .mapToInt(People::getAge)
                .average().ifPresent(System.out::println);
        System.out.println("----------------------------------------");
        System.out.println("Find the number of potentially able-bodied people in the sample " +
                           "(from 18 years, women retire at 55 and men at 60)");
        System.out.println(peopleList.stream()
                .filter(people -> (people.getSex() == Sex.MAN && people.getAge() >= 18 && people.getAge() <= 60)
                                  || (people.getSex() == Sex.WOMAN && people.getAge() >= 18 && people.getAge() <= 55))
                .count());
        System.out.println("----------------------------------------");
        System.out.println("Sort a collection of people by name in reverse alphabetical order: ");
        peopleList.stream()
                .sorted(Comparator.comparing(People::getName).reversed())
                .forEach(System.out::println);
        System.out.println("----------------------------------------");
        System.out.println("Sort people's collection first by name and then by age: ");
        peopleList.stream()
                .sorted(Comparator.comparing(People::getName).thenComparing(People::getAge))
                .forEach(System.out::println);
        System.out.println("----------------------------------------");
        System.out.println("Find the oldest person: ");
        System.out.println(peopleList.stream().max(Comparator.comparing(People::getAge)).get());
        System.out.println("----------------------------------------");
        System.out.println("Find the youngest person: ");
        System.out.println(peopleList.stream().min(Comparator.comparing(People::getAge)).get());
        System.out.println("----------------------------------------");
        System.out.println("Find out how many men there are: ");
        System.out.println(peopleList.stream()
                .filter(people -> people.getSex() == Sex.MAN).count());
        System.out.println("----------------------------------------");
        System.out.println("Find out how many woman there are: ");
        System.out.println(peopleList.stream()
                .filter(people -> people.getSex() == Sex.WOMAN).count());
        System.out.println("----------------------------------------");
        System.out.println("List all women whose name begins with \"A\":");
        peopleList.stream().filter(people -> people.getSex() == Sex.WOMAN && people.getName().startsWith("A")).forEach(System.out::println);
    }
}
