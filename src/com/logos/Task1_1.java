package com.logos;

import java.util.Scanner;

public class Task1_1 {
    public static void main(String[] args) {
        System.out.println("Вітаємо! введіть будь ласка суму депозиту: ");
        Scanner scanner = new Scanner(System.in);
        int amountOfMoney = scanner.nextInt();
        System.out.println("Введіть будь ласка процентну ставку: ");
        double interestRate = scanner.nextDouble();
        System.out.println("Введіть будь ласка термін депозиту в роках: ");
        int depositDuration = scanner.nextInt();
        int finalIncome = (int) (amountOfMoney * Math.pow((1+interestRate/100),depositDuration));
        System.out.println("Ваш внесок " +amountOfMoney+ " через "+ depositDuration+ " років буде складати "+finalIncome);
    }
}
