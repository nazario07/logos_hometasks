package com.logos;

public class Circle {
    float radius;
    float diameter;

    public  Circle(float radius,float diameter){
        this.radius = radius;
        this.diameter = diameter;
    }

    public void squareOfCircle(){
        float squareCircle = (float) (Math.PI *Math.pow(diameter/2,2));
        System.out.println("Площа кола = "+ squareCircle);
    }
    public void  lengthOfCircle(){
        float lengthCircle = (float) (Math.PI *2*radius);
        System.out.println("Довжина кола = "+ lengthCircle);
    }

    public static void main(String[] args) {
        Circle circle = new Circle(10,20);

        circle.squareOfCircle();
        circle.lengthOfCircle();

    }
}
