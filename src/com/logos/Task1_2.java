package com.logos;

import java.util.Arrays;
import java.util.Scanner;

public class Task1_2 {
    public static void main(String[] args) {
        System.out.println("Please, enter the array: ");
        int[] array = new int[10];
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        System.out.println(Arrays.toString(array));
        int[] resArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            resArray[i] = array[array.length-1-i];
        }
        System.out.println( Arrays.toString(resArray));
    }
}
