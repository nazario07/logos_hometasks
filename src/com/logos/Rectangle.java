package com.logos;

public class Rectangle {

    int length;
    int width;

    public Rectangle(int length, int width) { //конструктор з параметрами
        this.length = length;
        this.width = width;
    }
    public Rectangle(){   //конструктор без параметрів (за замовчуванням)
    }


    public static void main(String[] args) {
        //створимо прямокутник через конструктор з параметрами
        Rectangle rectangle1 = new Rectangle(100,50);


        //створимо прямокутник через конструктор без параметрів
        Rectangle rectangle2 = new Rectangle();
        rectangle2.length = 20;
        rectangle2.width = 10;

        System.out.println("Перший прямокутник: ");
        rectangle1.squareRectangle();
        rectangle1.perimeterRectangle();

        System.out.println("\n");

        System.out.println("Другий прямокутник: ");
        rectangle2.squareRectangle();
        rectangle2.perimeterRectangle();


    }


    public void squareRectangle(){
        int square = length*width;
        System.out.println("Площа прямокутника = "+ square);

    }
    public void perimeterRectangle(){
        int perimeter = 2*(length+width);
        System.out.println("Периметр прямокутника = "+ perimeter);
    }

}