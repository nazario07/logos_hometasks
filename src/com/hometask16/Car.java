package com.hometask16;

import java.util.Objects;

public class Car {
    private String brand;
    private String model;
    private int yearOfProduction;
    private int power;
    private int maxSpeed;
    private double price;

    public Car() {
    }

    public Car(String brand, String model, int yearOfProduction, int power, int maxSpeed, double price) {
        this.brand = brand;
        this.model = model;
        this.yearOfProduction = yearOfProduction;
        this.power = power;
        this.maxSpeed = maxSpeed;
        this.price = price;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return yearOfProduction == car.yearOfProduction && power == car.power && maxSpeed == car.maxSpeed && price == car.price && Objects.equals(brand, car.brand) && Objects.equals(model, car.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(brand, model, yearOfProduction, power, maxSpeed, price);
    }

    @Override
    public String toString() {
        return "Car{" +
                "brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", yearOfProduction=" + yearOfProduction +
                ", power=" + power +
                ", maxSpeed=" + maxSpeed +
                ", price=" + price +
                '}';
    }

    public void discountPrice() {
        price = (int) price * 0.8;
    }

    public void stageOfPower(int stageNumber) {
        if (stageNumber == 1) {
            power += 100;
            maxSpeed += 50;
            price += 10000;
        }
        if (stageNumber == 2) {
            power += 200;
            maxSpeed += 100;
            price += 20000;
        }
    }
}
