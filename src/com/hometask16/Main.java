package com.hometask16;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) {
        Car mercedes = new Car("Mercedes", "McLauren", 2020, 450, 330, 100000);
        try {
            getInfo(mercedes);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static void getInfo(Car mercedes) throws IllegalAccessException {
        Class<? extends Car> mirror = mercedes.getClass();

        Field[] declaredFields = mirror.getDeclaredFields();
        for (Field field : declaredFields) {
            field.setAccessible(true);
            System.out.println("Name of field " + field.getName() + "; Value " + field.get(mercedes));
        }

        System.out.println();
        Method[] declaredMethods = mirror.getDeclaredMethods();
        for (Method method : declaredMethods) {
            System.out.println("Name of method " + method.getName());
        }

        System.out.println("\nCars constructors: ");
        Constructor<?>[] constructors = mirror.getDeclaredConstructors();
        for (Constructor<?> constructor : constructors) {
            System.out.println(constructor);
        }

        System.out.println("\nCreate new objects: ");
        try {
            Car car1 = (Car) mirror.getDeclaredConstructors()[0].newInstance();
            System.out.println(car1);
        } catch (InvocationTargetException | InstantiationException e) {
            e.printStackTrace();
        }
        try {
            Car car2 = (Car) mirror.getDeclaredConstructors()[1].newInstance("Mercedes", "G550", 2019, 450, 330, 100000);
            System.out.println(car2);
        } catch (InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }

        System.out.println("\nCalling methods:" +
                "1. When discount for regular customer ");
        try {
            mirror.getMethod("discountPrice").invoke(mercedes);
            System.out.println(mercedes);
        } catch (InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }

        System.out.println("2.Client want to update car to stage2: ");
        try {
            mirror.getMethod("stageOfPower", int.class).invoke(mercedes, 2);
            System.out.println(mercedes);
        } catch (InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
}
