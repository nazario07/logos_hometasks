"use strict";
function makeBuffer() {
     let text = '';

    return function (entered_text) {
        if (arguments.length == 0) {
            return text;
        }
        text += entered_text;
    }
}

function clear(buffer){
    buffer ='';
    return buffer
}

let buffer = makeBuffer();
buffer("You");
buffer(" should");
buffer(" learn");
buffer(" Java");
buffer("Script");

console.log(buffer())
alert(buffer()); // You should learn JavaScript

console.log(clear(buffer)); /* "" */

let buffer1 = makeBuffer();
buffer1("I");
buffer1(" want");
buffer1(" to");
buffer1(" find");
buffer1(" Java");
buffer1("-job");

console.log(buffer1())
alert(buffer1()); // I want to find Java-job

console.log(clear(buffer1)); /* "" */

let buffer2 = makeBuffer();
buffer2(9)
buffer2(1)
buffer2(1)
buffer2(" is")
buffer2(" Emergency!")

console.log(buffer2())
alert(buffer2()); // 911 is Emergency!
console.log(clear(buffer2));  /* "" */