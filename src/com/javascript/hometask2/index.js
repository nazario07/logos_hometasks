let person = {};

person.name = "Пилип";
person.surname = "Шевченко";
console.log(person);
person.name = "Сергій"
console.log(person);
delete person.name;
console.log(person);


task2();

function task2() {
    let employeeSalaries = {};
    employeeSalaries.Andrew = 1000;
    employeeSalaries.Oksana = 2311;
    employeeSalaries.Nikol = 2000;
    employeeSalaries.Ostap = 3000;
    employeeSalaries.Nazar = 4020;
    let summa = 0;
    for (let employeeSalariesKey in employeeSalaries) {
        console.log(employeeSalariesKey + ": " + employeeSalaries[employeeSalariesKey])
        summa += employeeSalaries[employeeSalariesKey];
    }
    console.log("The amount of salary: " + summa + "$")
}


task3();

function task3() {
    let num1 = Number(prompt("Enter 1-st number: "));
    if (isNaN(num1)) {
        alert("You entered not a number");
        throw new Error("You entered not a number");
    }
    let action = prompt("Enter action: + - / * ");
    let num2 = Number(prompt("Enter 2-nd number: "));
    if (isNaN(num2)) {
        alert("You entered not a number");
        throw new Error("You entered not a number");
    }
    calculate(num1, action, num2);
}

function calculate(num1, action, num2) {
    let result;
    if (action === "+") {
        result = num1 + num2;
    }
    if (action === "-") {
        result = num1 - num2;
    }
    if (action === "/") {
        result = num1 / num2;
        if (num2 === 0) {
            throw new Error("!!! You can not divide by 0")
        }
    }
    if (action === "*") {
        result = num1 * num2;
    }
    if (action !== "+" && action !== "-" && action !== "/" && action !== "*") {
        alert("You entered incorrect symbol");
        throw new Error("You entered incorrect symbol!");
    }
    console.log(num1 + " " + action + " " + num2 + " = " + result)
}