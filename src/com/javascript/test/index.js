function add() {
    let number_1 = Number(document.getElementById("number_1").value);
    let number_2 = Number(document.getElementById("number_2").value);
    document.getElementById("result").innerText = number_1 + number_2;
}

function subtraction() {
    let number_1 = Number(document.getElementById("number_1").value);
    let number_2 = Number(document.getElementById("number_2").value);
    document.getElementById("result").innerText = number_1 - number_2;
}

function multiply() {
    let number_1 = Number(document.getElementById("number_1").value);
    let number_2 = Number(document.getElementById("number_2").value);
    document.getElementById("result").innerText = number_1 * number_2;
}

function divide() {
    let number_1 = Number(document.getElementById("number_1").value);
    let number_2 = Number(document.getElementById("number_2").value);
    if (number_2 === 0) {
        document.getElementById("result").innerText = "Error: you can not divide by 0!"
    } else
        document.getElementById("result").innerText = number_1 / number_2;
}

function powX_Y() {
    let number_1 = Number(document.getElementById("number_1").value);
    let number_2 = Number(document.getElementById("number_2").value);
    document.getElementById("result").innerText = Math.pow(number_1, number_2);

}

function sqrt() {
    let number_1 = Number(document.getElementById("number_1").value);
    document.getElementById("result").innerText = Math.sqrt(number_1);
    document.getElementById("number_2").value = "";
}

function cbrt() {
    let number_1 = Number(document.getElementById("number_1").value);
    document.getElementById("result").innerText = Math.cbrt(number_1);
}

let tooltipElem;

document.onmouseover = function (event) {
    let target = event.target;
    let tooltipHtml = target.dataset.tooltip;
    if (!tooltipHtml) return;

    tooltipElem = document.createElement('div');
    tooltipElem.className = 'tooltip';
    tooltipElem.innerHTML = tooltipHtml;
    document.body.append(tooltipElem);
}

document.onmouseout = function () {
    if (tooltipElem) {
        tooltipElem.remove();
        tooltipElem = null;
    }
}