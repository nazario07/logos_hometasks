task1();

function task1(){
    let num = prompt("Task1. Enter the number");
    console.log(num);
    if (!num) {
        alert('Canceled');
        return;
    }

    let i = 0;
    let sum = 0;
    while (i <= num) {
        sum = sum + i;
        i++;
    }
    alert('Sum of ' + num + ' first digit is ' + sum);
}


task2();

function task2(){
    let num = prompt("Task2. Enter the number");
    console.log('You enter '+ num);
    if (!num) {
        alert('Canceled');
        return;
    }

    if(num>0){
        alert( num+' is positive number');
        return;
    }

    if(num<0){
        alert( num+' is negative number');
        return;
    }

    alert( num+' is null');

}
task3();
function task3(){
    let num = prompt("Task3. 10*10 = ? ");
    console.log('Are you sure that 10*10 = '+ num);
    if (!num) {
        alert('Canceled');
        return;
    }

    while (num != 10 * 10) {
        num = prompt("Error! Try again: 10*10 = ? ");
        console.log(num);

        if (!num) {
            alert('Canceled');
        }
    }

    alert('All right! 10*10= ' + num);
}

task4();
 function task4() {
     let password = prompt("Enter the password");

     if (!password) {
         alert('Canceled');
         return;
     }

     while ( password !== 'pass123' && password !== "admin" ) {
          password = prompt('Password is incorrect! Try again! Enter the password:');
         if (!password) {
             alert('Canceled');
             return;
         }
     }

     if (password === "pass123") {
         alert('Password is correct. Enter is completed!');
         return;
     }

     alert('You are login like admin!');

 }

 task5();
 function task5(){
     let quantity = prompt("Enter the quantity fabonacci digit:");

     if (!quantity || quantity==0) {
         alert('Canceled');
         return;
     }

     let fibonacci = [0,1];
     for ( let i = 2; i <= quantity ; i++) {
         fibonacci[i]=fibonacci[i-1]+fibonacci[i-2]
     }
     alert(fibonacci.slice(1,quantity+1))

 }
