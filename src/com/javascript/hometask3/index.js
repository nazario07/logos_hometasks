task1();

function task1() {
    let products = ["Apple", "Orange", "Tomato", "Cucumber", "Potato"];
    alert("The last product in array is : " + products[products.length - 1])
    console.log(products[products.length - 1]);
}

task2();

function task2() {
    const styles = ["Jazz", "Bluez"];
    console.log(styles);

    styles.push("Rock-n-Roll");
    console.log(styles);

    styles[styles.length - 2] = "Classic";
    console.log(styles);

    let s = styles.shift();
    console.log(s);

    styles.unshift("Rep", "Reggi");
    console.log(styles);
}

task3_find([1, 2, 3, 4, 5, 6], 4);

function task3_find(arr, value) {
    let index;
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] === value) {
            index = i;
            console.log(value + ' is ' + index + "-th element");
            alert(value + ' is ' + index + "-th element");
        }
    }
    if (isNaN(index)) {
        console.log(-1);
        alert(-1);
    }
}

task4_filterRange([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15], 1, 5);

function task4_filterRange(array, a, b) {
    if (a < array.length && b < array.length && a < b) {
        let slice = array.slice(a, b + 1);
        console.log(slice);
        return slice;
    } else {
        throw new Error("Something went wrong")
    }
}

task5_camelize("my-short-string");

function task5_camelize(str) {
    let split_array = str.split("-");
    console.log(split_array)
    for (let i = 0; i < split_array.length; i++) {
        // split_array[i] = split_array[i][0].toUpperCase() + split_array[i].substring(1);
        split_array[i] = split_array[i].charAt(0).toUpperCase() + split_array[i].slice(1);
    }
    let result = split_array.join('');
    console.log(result)
    return result;
}