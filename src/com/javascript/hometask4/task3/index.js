const group = [];
studentAdd(20, 'Java');
studentAdd(18, 'JavaScript');
studentAdd(19, 'Java');
studentAdd(22, 'C#');
studentAdd(21, 'Java');
studentAdd(17, 'Ruby');
studentAdd(18, 'Kotlin');
studentAdd(19, 'Java');
studentAdd(16, 'Ruby');
studentAdd(20, 'C#');
console.log(group);

createArrayOfGroupName(group);
sumOfAgeOfStudent1(group);
checkOf18Age(group);


function studentAdd(age, groupName) {
    const student = {
        age,
        groupName
    }
    group.push(student);
}

function createArrayOfGroupName(array) {
    const arrayGroupName = [];
    for (const student of array) {
        arrayGroupName.push(student.groupName);
    }
    console.log(arrayGroupName)
    return arrayGroupName;
}

// function sumOfAgeOfStudent(array) {
//
//     let sumOfAge = 0;
//     for (const student of array) {
//         sumOfAge += student.age;
//     }
//     console.log("Sum of student`s age is: " + sumOfAge);
//     return sumOfAge;
// }

function sumOfAgeOfStudent1(array) {
    let sumOfAge = array.reduce(function (accum, student) {
        return accum + student.age;
    }, 0);
    console.log("Sum of student`s age is: " + sumOfAge);
}

function checkOf18Age(array) {
    let countOfChildren = 0;
    for (const student of array) {
        if (student.age < 18) {
            countOfChildren++;
        }
    }
    console.log(`There are ${countOfChildren} not adult in the group`);
}