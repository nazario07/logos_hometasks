const array = [-2, 4, 7, 9, -2, 0, -7, 4, 6, 9, -22, 54, 100, 30, -54, 97, 9, -9, 18, -20];

const arraySortedAsc = array.sort(sortAsc);
console.log("Array sorted from smallest to largest:");
console.log(arraySortedAsc);
document.getElementById("asc").innerHTML = arraySortedAsc.toString();

const arraySortedDesc = array.sort(sortDesc);
console.log("Array sorted from largest to smallest:");
console.log(arraySortedDesc);
document.getElementById("desc").innerHTML = arraySortedDesc.toString();

function sortAsc(a, b) {
    if (a < b) return -1;
    if (a > b) return 1;
}

function sortDesc(a, b) {
    if (a < b) return 1;
    if (a > b) return -1;
}

const positive = array.filter(positiveArray);
console.log(positive);
document.getElementById("positive").innerHTML = positive.toString();

const negative = array.filter(negativeArray);
console.log(negative);
document.getElementById("negative").innerHTML = negative.toString();

function positiveArray(a) {
    return a > 0;
}

function negativeArray(a) {
    return a < 0;
}