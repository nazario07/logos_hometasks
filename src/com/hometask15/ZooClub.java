package com.hometask15;

import java.util.*;

public class ZooClub {
    public static void main(String[] args) {
        System.out.println("Hello! Welcome in our ZOO CLUB!");
        Scanner scanner = new Scanner(System.in);
        Map<Person, List<Animal>> zooClubMap = new HashMap<>();
        List<Person> personList = new ArrayList<>();
        List<List<Animal>> animalLists = new ArrayList<>();

        boolean proceed = true;
        while (proceed) {
            System.out.println("Please choose action: \n" +
                    "1-add new person, " +
                    "2-add new animal for person, " +
                    "3-remove the animal of person, " +
                    "4-remove person from club, " +
                    "5-print the zooClub, " +
                    "other num - exit of program.");
            int tmp = scanner.nextInt();
            switch (tmp) {
                case (1) -> {
                    System.out.println("Enter the name and age :");
                    personList.add(new Person(scanner.next(), scanner.nextInt()));
                    animalLists.add(new ArrayList<>());
                }

                case (2) -> {
                    boolean pro = true;
                    do {
                        System.out.println("What name have a person, whom you want to add animal?");
                        String namePersonToAddAnimal = scanner.next();
                        int index;
                        for (int i = 0; i < personList.size(); i++) {
                            if (namePersonToAddAnimal.equalsIgnoreCase(personList.get(i).getName())) {
                                index = i;
                                System.out.println("How much animal you want to add?");
                                int count = scanner.nextInt();
                              //  animalLists.add(index, new ArrayList<>());
                                for (int j = 0; j < count; j++) {
                                    System.out.println("Enter the type and name of animal:");
                                    animalLists.get(index).add(new Animal(scanner.next(), scanner.next()));
                                }
                                pro = false;
                            } else
                                System.out.println();
                        }
                    }
                    while (pro);
                }

                case (3) -> {
                    System.out.println("Enter the name of animal that you want delete:");
                    String animalForDelete = scanner.next();
                    for (List<Animal> animalList : animalLists) {
                        for (int j = 0; j < animalList.size(); j++) {
                            if (animalList.get(j).getNameOfAnimal().equalsIgnoreCase(animalForDelete)) {
                                animalList.remove(j);
                            }
                        }
                    }
                }
                case (4) -> {
                    System.out.println("Enter the name of person what you want delete:");
                    String nameOfPersonForDelete = scanner.next();
                    for (int i = 0; i < personList.size(); i++) {
                        if (personList.get(i).getName().equalsIgnoreCase(nameOfPersonForDelete)) {
                            zooClubMap.remove(personList.get(i));
                            personList.remove(i);
                            animalLists.remove(i);
                        }
                    }
                }

                case (5) -> {
                    for (int i = 0; i < personList.size(); i++) {
                        zooClubMap.put(personList.get(i), animalLists.get(i));
                    }
                    for (Map.Entry<Person, List<Animal>> entry : zooClubMap.entrySet()) {
                        System.out.println(entry.getKey() + "" + "" + entry.getValue());
                    }
                }
                default -> proceed = false;
            }
        }
    }

}


