package com.hometask13_1;

public class User implements Comparable <User> {
    private String userName;
    private int userAge;
    private String userEmail;

    public User(String userName, int userAge, String userEmail) {
        this.userName = userName;
        this.userAge = userAge;
        this.userEmail = userEmail;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getUserAge() {
        return userAge;
    }

    public void setUserAge(int userAge) {
        this.userAge = userAge;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    @Override
    public String toString() {
        return "User{" +
                "userName='" + userName + '\'' +
                ", userAge=" + userAge +
                ", userEmail='" + userEmail + '\'' +
                '}';
    }

    @Override
    public int compareTo(User user) {
        if (this.userName.compareTo(user.getUserName()) > 0) {
            return 1;
        } else if (this.userName.compareTo(user.getUserName()) < 0) {
            return -1;
        } else {
            if (this.userAge > user.getUserAge()) {
                return 1;
            } else if (this.userAge < user.getUserAge()) {
                return -1;
            }else{
                if(this.userEmail.compareTo(user.getUserEmail())>0){
                    return 1;
                } else if(this.userEmail.compareTo(user.getUserEmail())<0){
                    return -1;
                }
            }
        }

        return 0;
    }
}
