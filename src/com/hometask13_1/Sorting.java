package com.hometask13_1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Sorting {
    public static void main(String[] args) {
        List<User> userList = new ArrayList<>();
        userList.add(new User("Alex", 27, "a@gmail.com"));
        userList.add(new User("Alex", 23, "le@gmail.com"));
        userList.add(new User("Alex", 23, "alex21@gmail.com"));
        userList.add(new User("John", 19, "john18@gmail.com"));
        userList.add(new User("John", 18, "2johnsssssss18@gmail.com"));
        userList.add(new User("John", 18, "1john18@gmail.com"));
        userList.add(new User("Sahra", 29, "sahra27@gmail.com"));
        userList.add(new User("Sahra", 27, "3sahra27@gmail.com"));
        userList.add(new User("Sahra", 27, "2sahra27@gmail.com"));
        userList.add(new User("Mikal", 25, "mikal23@gmail.com"));
        userList.add(new User("Mikal", 23, "4mikal23@gmail.com"));
        userList.add(new User("Mikal", 23, "3mikal23@gmail.com"));

//        Collections.sort(userList);
//        Iterator<User> userIterator = userList.iterator();
//        while(userIterator.hasNext()){
//            User user = userIterator.next();
//            System.out.println(user);
//    }

        //sort by comparator
        Collections.sort(userList, new ComparatorLengthMail());
        Iterator<User> userIterator = userList.iterator();
        while (userIterator.hasNext()) {
            User user = userIterator.next();
            System.out.println(user);

        }
    }
}
