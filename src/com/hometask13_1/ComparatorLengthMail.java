package com.hometask13_1;

import java.util.Comparator;

public class ComparatorLengthMail implements Comparator<User> {
    @Override
    public int compare(User user1, User user2) {
        return user1.getUserEmail().length() > user2.getUserEmail().length() ? 1: -1;
    }
}
