package com.hometask13_2;

import java.util.Comparator;

public abstract class Comparators implements Comparator<Commodity> {

    public static Comparators getComparatorByName() {
        Comparators comparatorByName = new Comparators() {
            @Override
            public int compare(Commodity commodity1, Commodity commodity2) {
                return commodity1.getNameOfProduct().compareTo(commodity2.getNameOfProduct());
            }
        };
        return comparatorByName;
    }

    public static Comparators getComparatorByLength() {
        Comparators comparatorByLength = new Comparators() {
            @Override
            public int compare(Commodity commodity1, Commodity commodity2) {
                return commodity1.getLength() > commodity2.getLength() ? 1 : -1;
            }
        };
        return comparatorByLength;
    }

    public static Comparators getComparatorByWeight() {
        Comparators comparatorByWeight = new Comparators() {
            @Override
            public int compare(Commodity commodity1, Commodity commodity2) {
                return commodity1.getWeight() > commodity2.getWeight() ? 1 : -1;
            }
        };
        return comparatorByWeight;
    }

    public static Comparators getComparatorByWidth() {
        Comparators comparatorByWidth = new Comparators() {
            @Override
            public int compare(Commodity commodity1, Commodity commodity2) {
                return commodity1.getWidth() > commodity2.getWidth() ? 1 : -1;
            }
        };
        return comparatorByWidth;
    }

}
