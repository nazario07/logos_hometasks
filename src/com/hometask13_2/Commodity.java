package com.hometask13_2;

public class Commodity {
    private String nameOfProduct;
    private double length;
    private double weight;
    private double width;

    public Commodity(String nameOfProduct, double length, double weight, double width) {
        this.nameOfProduct = nameOfProduct;
        this.length = length;
        this.weight = weight;
        this.width = width;

    }

    public String getNameOfProduct() {
        return nameOfProduct;
    }

    public void setNameOfProduct(String nameOfProduct) {
        this.nameOfProduct = nameOfProduct;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }



    @Override
    public String toString() {
        return "Commodity{" +
                "nameOfProduct='" + nameOfProduct + '\'' +
                ", length=" + length +
                ", weight=" + weight +
                ", width=" + width +
                '}';
    }

    public void changeOfProduct(String newNameOfProduct, double newLength, double newWeight, double newWidth){
        this.setNameOfProduct(newNameOfProduct);
        this.setLength(newLength);
        this.setWeight(newWeight);
        this.setWidth(newWidth);
    }
}
