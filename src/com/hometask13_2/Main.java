package com.hometask13_2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        List<Commodity> commodityList = new ArrayList<>();
        System.out.println("Hello! ");
        boolean check = true;
        do {
            System.out.println("""
                    Choose action:  1 - If you want add the product\s
                    2 - remove the product  3 - change the product\s
                    4 - sorting by name of product  5 - sorting by length of product
                    6 - sorting by weight of product  7 - sorting by width of product
                    8 - print entered product by some index  0 - exit from program
                    9 - print all commodity""");
            int tmp = scanner.nextInt();
            switch (tmp) {
                case (1) -> {
                    System.out.println("Enter name/length/weight/width: ");
                    commodityList.add(new Commodity(scanner.next(), scanner.nextDouble(), scanner.nextDouble(), scanner.nextDouble()));
                }
                case (2) -> {
                    System.out.println("Enter the number of product for delete:");
                    commodityList.remove(scanner.nextInt());
                }
                case (3) -> {
                    System.out.println("Enter the number of product for change:");
                    commodityList.get(scanner.nextInt()).changeOfProduct(scanner.next(), scanner.nextInt(), scanner.nextInt(), scanner.nextInt());
                }
                case (4) -> {
                    commodityList.sort(Comparators.getComparatorByName());
                    System.out.println("Sorting by name: ");
                    for (Commodity commodity : commodityList) {
                        System.out.println(commodity);
                    }
                }
                case (5) -> {
                    commodityList.sort(Comparators.getComparatorByLength());
                    System.out.println("Sorting by length: ");
                    for (Commodity commodity : commodityList) {
                        System.out.println(commodity);
                    }
                }
                case (6) -> {
                    commodityList.sort(Comparators.getComparatorByWeight());
                    System.out.println("Sorting by weight: ");
                    for (Commodity commodity : commodityList) {
                        System.out.println(commodity);
                    }
                }
                case (7) -> {
                    commodityList.sort(Comparators.getComparatorByWidth());
                    System.out.println("Sorting by width: ");
                    for (Commodity commodity : commodityList) {
                        System.out.println(commodity);
                    }
                }
                case (8) -> {
                    System.out.println("Enter the number of product for print:");
                    System.out.println(commodityList.get(scanner.nextInt()));
                }
                case (9) -> {
                    for (Commodity commodity : commodityList) {
                        System.out.println(commodity);
                    }
                }
                case (0) -> {
                    System.out.println("Good luck! Come back!");
                    System.exit(0);
                    check = false;
                }
                default -> System.out.println("You entered incorrect number. Please try again.");
            }

        }
        while (check);
        for (Commodity commodity : commodityList) {
            System.out.println(commodity);
        }
    }
}
