package com.hometask18;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.*;

import static com.hometask18.CustomReader.readNextInteger;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MyEntry<K, V> {
    K id;
    V nameAndSurname;

    Map<K, V> map = new HashMap<>();

    public void addNewElement(K id, V surname) {
        map.put(id, surname);
    }

    public void deleteById(K id) {
        System.out.println("Delete element with id  " + id);
        map.remove(id);
    }

    public void deleteByNameSurname(V surname) {
        System.out.println("Delete element who is " + surname);
        Set<Map.Entry<K, V>> entrySet = map.entrySet();
        entrySet.removeIf(kvEntry -> surname.equals(kvEntry.getValue()));
    }

    public void changeByKey(K id) {
        System.out.println("Enter the value, that you want add: ");
        V str = (V) readNextInteger();
        Set<Map.Entry<K, V>> entrySet = map.entrySet();
        K keyForChange = null;
        for (Map.Entry<K, V> pair : entrySet) {
            if (id.equals(pair.getKey())) {
                keyForChange = pair.getKey();
            }
        }
        map.put(keyForChange, str);
    }

    public void showKeySet() {
        Set<K> keys = map.keySet();
        for (K key : keys) {
            System.out.println(key);
        }
    }

    public void showEntryList() {
        List<V> entryList = new ArrayList<>(map.values());
        for (V entry : entryList) {
            System.out.println(entry);
        }
    }

    public void showMap() {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }
    }
}
