package com.hometask18;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CustomReader {

    public static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    private static String readLine() {
        String input = null;
        try {
            input = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return input;
    }

    public static Integer readNextInteger() {
        String input = readLine();
        Integer result = null;
        try {
            result = Integer.parseInt(input);
        } catch (NumberFormatException e) {
            System.out.println("You enter incorrect number. Try again! Your value is " + input);
            result = readNextInteger();
        }
        return result;
    }

    public static String readNextString() {
        return readLine();
    }
}
