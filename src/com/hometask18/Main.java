package com.hometask18;


import static com.hometask18.CustomReader.readNextInteger;
import static com.hometask18.CustomReader.readNextString;

public class Main {
    public static void main(String[] args) {

        MyEntry<Integer, String> myEntry = new MyEntry<>();
        System.out.println("Hello! Welcome to the our MAP!");
        boolean proceed = true;
        while (proceed) {
            System.out.println("""
                    Please,choose action: 1 - add new member\s
                    2 - change element by id | 3 - delete element by id,\s
                    4 - delete element by name & surname | 5 - show set of keys\s
                    6 - show list of values | 7 - show all map | other num - exit.""");
            int choose = readNextInteger();
            switch (choose) {
                case (1) -> {
                    System.out.println("Enter id, name and surname: ");
                    myEntry.addNewElement(readNextInteger(), readNextString());
                }
                case (2) -> {
                    System.out.println("Enter id, which ou want to change:");
                    myEntry.changeByKey(readNextInteger());
                }
                case (3) -> {
                    System.out.println("Enter id of person which you want to delete: ");
                    myEntry.deleteById(readNextInteger());
                }
                case (4) -> {
                    System.out.println("Enter name of person which you want to delete: ");
                    myEntry.deleteByNameSurname(readNextString());
                }
                case (5) -> {
                    System.out.println("This is set of keys: ");
                    myEntry.showKeySet();
                }
                case (6) -> {
                    System.out.println("This is list of values: ");
                    myEntry.showEntryList();
                }
                case (7) -> {
                    System.out.println("This is our MAP: ");
                    myEntry.showMap();
                }
                default -> {
                    proceed = false;
                    System.out.println("See you later! Good luck!");
                }
            }
        }
    }
}
